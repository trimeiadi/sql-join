Soal 1 Mengambil Data dari Database
a. Mengambil data users
   SELECT id, name, email FROM users;

b. Mengambil data items
   SELECT * FROM items WHERE price > 1000000;

   SELECT * FROM items WHERE name LIKE "%Watch";

c. Menampilkan data items join dengan kategori
   SELECT items.name, items.description, items.price, items.stok, items.category_id, categories.name AS kategori 
   FROM items INNER JOIN categories ON items.category_id = categories.id;

Soal 2 Mengubah Data dari Database
UPDATE items SET price = 2500000 WHERE name = 'Sumsang b50';